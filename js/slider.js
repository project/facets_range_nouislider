/**
 * @file
 * Provides the slider functionality.
 */

(function () {

  'use strict';

  Drupal.facets = Drupal.facets || {};

  Drupal.behaviors.facet_slider = {
    attach: function (context, settings) {
      if (settings.facets !== 'undefined' && settings.facets.sliders !== 'undefined') {
        for (const [facet, facet_settings] of Object.entries(settings.facets.sliders)) {
          // Target facet element from context.
          const facet_elements = once('#' + facet, '.facet-slider');
          facet_elements.forEach((facet_element) => {
            if (facet_element) {
              Drupal.facets.addSlider(facet_element, facet_settings);
            }
          });
        }
      }
    }
  };


  Drupal.facets.addSlider = function (facet, settings) {
    // Slider inputs functionality.
    const input0 = document.getElementById('nouislider-input-from');
    const input1 = document.getElementById('nouislider-input-to');
    const inputs = input1 ? [input0, input1] : [input0];

    const defaults = {
      change: function (values, handle, unencoded, tap, positions, noUiSlider) {
        if (settings.range) {
          window.location.href = settings.url.replace('__range_slider_min__', Math.round(unencoded[0])).replace('__range_slider_max__', Math.round(unencoded[1]));
        }
        else {
          window.location.href = settings.urls['f_' + values[0]];
          window.location.href = settings.urls['f_' + values[0]];
        }
      }
    };

    settings = Object.assign({}, defaults, settings);

    const start = typeof settings.value === 'undefined' || settings.value === null ? [settings.values[0], settings.values[1]] : settings.value;

    const format = {
      to: (value) => {
        return settings.prefix + Math.round(value) + settings.suffix;
      },
      from: (value) => {
        return value;
      }
    };
    let pips = {
      mode: settings.pips_mode,
      values: settings.pips_values,
      density: settings.pips_density
    };
    const tooltips = settings.tooltips ? format : false;
    pips = settings.pips ? pips : false;

    const sliderSettings = {
      range: {
        min: settings.min,
        max: settings.max
      },
      step: settings.step,
      start: start,
      connect: true,
      tooltips: tooltips,
      pips: pips
    };


    noUiSlider.create(facet, sliderSettings);

    let timer, updateTimer = (el, handle, settings) => {

      let value = el.value;

      if (settings.range) {
        value = handle === 0 ? [value, null] : [null, value];
      }
      facet.noUiSlider.set(value);
    };

    if (settings.add_inputs) {
      facet.noUiSlider.on('update', (values, handle) => {
        inputs[handle].value = values[handle];
      });

      // Listen to keydown events on the input field.
      inputs.forEach((input, handle) => {
        input.addEventListener('change', function () {
          facet.noUiSlider.setHandle(handle, this.value);
        });

        input.addEventListener('keydown', function (e) {

          const values = facet.noUiSlider.get(true);
          const value = values.constructor === Array ? values[handle] : values;
          const steps = facet.noUiSlider.steps();

          // [down, up]
          const step = steps[handle];

          let position;
          this.value = Number(this.value);

          switch (e.which) {

            // 13 is enter,
            case 13:
              updateTimer(this, handle, settings);
              break;

            // 38 is key up,
            case 38:

              // Get step to go increase slider value (up)
              position = step[1];

              // false = no step is set
              if (position === false) {
                position = 1;
              }

              // null = edge of slider
              if (position !== null) {
                this.value = Number(this.value) + position;
                clearTimeout(timer);
                timer = setTimeout(updateTimer, 1000, this, handle, settings);
              }


              break;
            // 40 is key down.
            case 40:

              position = step[0];

              if (position === false) {
                position = 1;
              }

              if (position !== null) {
                this.value = Number(this.value) - position;
                clearTimeout(timer);
                timer = setTimeout(updateTimer, 1000, this, handle, settings);
              }

              break;
          }
        });
      });
    }

    facet.noUiSlider.on('set', settings.change);

    // Make the noUiSlider object availble for sites to add their own handlers.
    window.facet_nouisliders = window.facet_nouisliders || {};
    window['facet_nouisliders'][facet.getAttribute('id')] = facet;
  };
})();
