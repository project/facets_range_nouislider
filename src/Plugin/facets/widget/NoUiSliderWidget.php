<?php

namespace Drupal\facets_range_nouislider\Plugin\facets\widget;

use Drupal\Core\Form\FormStateInterface;
use Drupal\facets\FacetInterface;
use Drupal\facets_range_widget\Plugin\facets\widget\SliderWidget;

/**
 * The NoUiSlider widget.
 *
 * @FacetsWidget(
 *   id = "nouislider",
 *   label = @Translation("NoUiSlider"),
 *   description = @Translation("A widget that shows a NoUiSlider."),
 * )
 */
class NoUiSliderWidget extends SliderWidget {

  /**
   * @return array
   */
  public function defaultConfiguration(): array {
    return [
        'add_inputs' => FALSE,
        'pips' => FALSE,
        'pips_mode' => 'range',
        'is_below' => TRUE,

      ] + parent::defaultConfiguration();
  }

  /**
   * @param FacetInterface $facet
   *
   * @return array
   */
  public function build(FacetInterface $facet): array {
    $build = parent::build($facet);

    if (empty($facet->getResults())) {
      return $build;
    }

    $facet_settings = &$build['#attached']['drupalSettings']['facets']['sliders'][$facet->id()];
    $facet_settings = array_merge($facet_settings, $this->getConfiguration());

    // Add inputs template.
    if ($facet_settings['add_inputs']) {

      $input = [
        '#type' => 'textfield',
        '#title' => $this->getConfiguration()['from'],
        '#attributes' => [
          'id' => [
            'nouislider-input-from',
          ],
          'class' => [
            'nouislider-input',
          ],
        ],
      ];

      $render = [
        '#theme' => 'facets_range_nouislider_form',
        '#inputs' => $input,
      ];

      $build['#items'][] = $render;

      if (!$facet_settings['is_below']) {
        $build['#items'] = array_reverse($build['#items']);
      }
    }
    // Alter library from Base class.
    $build['#attached']['library'][array_search('facets_range_widget/slider', $build['#attached']['library'])] = 'facets_range_nouislider/facets_range_nouislider';
    $build['#attributes']['class'][] = 'facet-nouislider';

    return $build;
  }

  /**
   * @param array $form
   * @param FormStateInterface $form_state
   * @param FacetInterface $facet
   *
   * @return array
   */
  public function buildConfigurationForm(array $form, FormStateInterface $form_state, FacetInterface $facet): array {
    $config = $this->getConfiguration();
    $form = parent::buildConfigurationForm($form, $form_state, $facet);

    $form['tooltips'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show tooltip / tooltips'),
      '#default_value' => $this->getConfiguration()['tooltips'],
    ];

    $form['pips'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Show pips'),
      '#default_value' => $this->getConfiguration()['pips'],
    ];

    $form['pips_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Pips mode'),
      '#default_value' => $this->getConfiguration()['pips_mode'],
      '#options' => [
        'range' => $this->t('Range mode'),
        'steps' => $this->t('Steps mode'),
        'positions' => $this->t('Positions mode'),
        'count' => $this->t('Count mode'),
        'values' => $this->t('Values mode'),
      ],
    ];

    $form['pips_values'] = [
      '#type' => 'number',
      '#title' => $this->t('Pips values'),
      '#default_value' => $this->getConfiguration()['pips_values'],
      '#min' => 0,
      '#step' => 1,
    ];

    $form['pips_density'] = [
      '#type' => 'number',
      '#title' => $this->t('Pips density'),
      '#default_value' => $this->getConfiguration()['pips_density'],
      '#min' => 0,
      '#max' => 100,
      '#step' => 1,
    ];

    $form['add_inputs'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Add form input / inputs to slider'),
      '#default_value' => $this->getConfiguration()['add_inputs'],
    ];

    $form['is_below'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Form input / inputs are below the slider. Otherwise above'),
      '#default_value' => $this->getConfiguration()['is_below'],
    ];

    $form['from'] = [
      '#type' => 'textfield',
      '#size' => 5,
      '#title' => $this->t('Label from'),
      '#default_value' => $config['from'],
    ];

    return $form;
  }

}
