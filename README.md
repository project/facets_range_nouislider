## CONTENTS OF THIS FILE

  - Requirements
  - Recommended Modules
  - Installation
  - Configuration
  - Features
  - Extension modules
  - FAQ
  - Maintainers


## INTRODUCTION

The Facets range NoUiSlider module allows site builders to easily set and configure NoUiSlider with themeable inputs fields.
Slider from Facets module requires Jquery Ui. NoUiSlider is light and configurable as you need. You can add input/inputs before or after slider.
You can configure value scale and other slider parameters. Input fields support key up & key down & enter from keyboard.

## REQUIREMENTS

Facets range widget from Facets module must be enabled.

## INSTALLATION

 * Install as you would normally install a contributed Drupal module. Visit:
   https://www.drupal.org/docs/extending-drupal/installing-modules
   for further information.

* If you want to use the sliders, you need to add the noUiSlider library
  from Asset Packagist using Composer.
  If you are using the Lightning or Drupal Commerce base distros, just run
  `composer require "npm-asset/nouislider:^15"`
  If you don't have Asset Packagist configured, see
  https://github.com/drupal-composer/drupal-project/issues/278#issuecomment-300714410
  for instructions.



## FEATURES
Lightweight NoUiSlider without JqueryUi with settings from Facets widget admin panel.
NoUiSlider
Range NoUiSlider

## MAINTAINERS

 * Vasiliy Repin - https://www.drupal.org/u/vasiliyrepin
